import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import * as serviceWorker from './serviceWorker';
import Header from './components/PageComponents/Header/Header'
import Footer from './components/PageComponents/Footer/Footer'
import { Route, Switch } from "react-router";
import { BrowserRouter } from 'react-router-dom';

import App from './pages/MessagePage/AuLinePage';
import HomePage from './pages/HomePage/HomePage';
import Page404 from './pages/404Page/404';

import axios from 'axios';

export var GlobalData = [];

let api = 
axios.create(
  {
      baseURL: 'https://wolfbook.herokuapp.com/'
  }
);
GlobalData['api'] = api;
GlobalData["login"] = "";
GlobalData["password"] = "";

ReactDOM.render(
  <React.StrictMode>
    <Header />

      <BrowserRouter>
        <Switch>

          <Route exact path = '/'>
            <HomePage />
          </Route>

          <Route exact path = '/register'>
            <HomePage registering={true} />
          </Route>

          <Route exact path = '/feed'>
            <App />
          </Route>

          <Route>
            <Page404 />
          </Route>

        </Switch>

      </BrowserRouter>

    <Footer />
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
