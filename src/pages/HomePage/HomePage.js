import React, {useState} from 'react';
import './HomePage.css';
import Info, {GlobalData} from '../../index';

function HomePage(props) {

    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");

    const login = () => {
        GlobalData['api'].post('/login');
    }

    const register = () => {
        if (!props.registering) {
            window.location.href = '/register';
        }
    }
    

    let form = [];
    const generateForm = () => {
        if (props.registering) {
            form.push(<input key={4} type="text" placeholder="Nome" />);
        }
        form.push(<input type="text" key={0} defaultValue={email} placeholder="Email" onChange={(e) => setEmail(e.target.value) } />);
        form.push(<input type="password" key={1} defaultValue={password} placeholder="Senha" onChange={(e) => setPassword(e.target.value)} />);
        if (props.registering) {
            form.push(<input key={8} type="password" placeholder="Confirmar senha" />)
            form.push(
            <select key={10}>
                <option key={5}>Masculino</option>
                <option key={6}>Feminino</option>
                <option key={7}>Outro</option>
            </select>
            );
            form.push(<input key={9} type="date" placeholder="Data de Nascimento" />);
        } else {
            form.push(<button key={2} onClick={login}>Logar</button>);
        }
        form.push(<button key={3} onClick={register}>Register</button>);
    }
    generateForm();

    return (
        <section className="home-section">
            {form}
        </section>
    )
}

export default HomePage;
